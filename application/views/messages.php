<div class="messages">
	<?php if($this->session->flashdata('danger')): ?>
		<?php echo '<script>alert("'.$this->session->flashdata('danger').'");</script>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')): ?>
		<?php echo '<script>alert("'.$this->session->flashdata('success').'");</script>'; ?>
	<?php endif; ?>
</div>