<div class="jumbotron jmt_header">
    <div class="container">
        <h1 class="display-4 yikes"><?php echo @$title; ?></h1>
    </div>
</div>
<?php echo form_open('user/logowanie') ;?>
<div class="container">
  <div class="row">
    <div class="offset-md-4 col-md-4">
      <h1 class="text-center">Logowanie</h1>
      <div class="form-group">
        <label for="user">Podaj login</label>
        <input type="text" name="user" id="user" class="form-control" placeholder="Podaj login" required="" autofocus="">
      </div>
      <div class="form-group">
        <label for="haslo">Podaj hasło</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Podaj hasło" required="">
        <p class="text-right"><a href="<?php echo base_url('user/reset_hasla'); ?>">nie pamiętam hasła</a></p>
      </div>
      <button type="submit" class="btn btn-primary btn-block">Zaloguj</button>
    </div>
  </div>
</div>
<?php echo form_close(); ?>