<?php
	class Rachunek_model extends CI_Model{
		public function login($user, $password){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user', $user);
			$this->db->where('password', $password);

			$result = $this->db->get();
			return $result->num_rows() == 1 ? $result->row_array() : false;
			
		}
		// public function check_user_exist($user){
		// 	$this->db->select('id');
		// 	$this->db->from('users');
		// 	$this->db->where('user', $user);
		// 	$result = $this->db->get();
		// 	return $result->num_rows()>0 ? true : false;
		// }
		// public function check_user_exist_by_data($data){
		// 	$this->db->select('id');
		// 	$this->db->from('users');
		// 	$this->db->where($data);
		// 	$result = $this->db->get();
		// 	return $result->num_rows()>0 ? true : false;
		// }
		// public function check_can_registered($idu){
		// 	$this->db->select('*');
		// 	$this->db->from('users');
		// 	$this->db->where('registered is null');
		// 	$this->db->where('id', $idu);
		// 	$count = $this->db->count_all_results();
		// 	return $count==1 ? true : false;
		// }
		// public function getUser($id=FALSE){
		// 	$this->db->select('
		// 		u.*, 
		// 		r.id as "idr", r.imie, r.nazwisko, r.imie2, r.nazwisko2, r.ulica, r.dom, r.kod, r.miejscowosc, r.telefon, r.telefon2
		// 	');
		// 	$this->db->from('users u');
		// 	$this->db->join('rodzice r', 'r.idu=u.id', 'left');
		// 	$this->db->order_by('u.lvl, u.registered', 'DESC');
		// 	if($id){
		// 		$this->db->where('u.id', $id);
		// 		$this->db->limit(1);
		// 		return $this->db->get()->row_array();
		// 	} else{
		// 		return $this->db->get()->result_array();
		// 	}
		// }
		// public function getUserByMail($mail){
		// 	$this->db->select('u.*, r.id as "idr"');
		// 	$this->db->from('users u');
		// 	$this->db->join('rodzice r', 'r.idu=u.id', 'left');
		// 	$this->db->where('u.mail', $mail);
		// 	$this->db->limit(1);
		// 	// return $this->db->get_compiled_select();
		// 	return $this->db->get()->row_array();
		// }
		// public function addUser($data){
		// 	$user = array(
		// 		'user' => $data['user'],
		// 		'password' => $data['password'],
		// 		'mail' => $data['mail'],
		// 	);
		// 	if($this->db->insert('users', $user)){
		// 		$idu = $this->db->insert_id();
		// 		$rodzic = array(
		// 			'idu' => $idu,
		// 			'imie' => $data['imie'],
		// 			'nazwisko' => $data['nazwisko'],
		// 		);
		// 		if($this->db->insert('rodzice', $rodzic)){
		// 			return array( 
		// 				'insert' => true,
		// 				'id' => $idu,
		// 			);
		// 		} else{
		// 			return array( 'insert' => false );
		// 		}
		// 	} else{
		// 		return array( 'insert' => false );
		// 	}
		// }
		// public function updUser($data, $id){
		// 	$this->db->limit(1);
		// 	$this->db->where('id', $id);
		// 	return $this->db->update('users', $data);
		// }
		// public function registerUser($user){
		// 	return $this->updUser(
		// 		array(
		// 			'registered' => 1, 
		// 			'password' => md5($user['password'])
		// 		), 
		// 		$user['id']
		// 	);
		// }
		// public function delUser($id){
		// 	$this->db->limit(1);
		// 	$this->db->where('id', $id);
		// 	return $this->db->delete('users');
		// }
	}