<?php
	class User_model extends CI_Model{
		public function user_get($where){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where($where);
			return $this->db->get()->result_array();
		}
		public function user_upd($data, $id){
			$this->db->limit(1);
			$this->db->where('id', $id);
			return $this->db->update('users', $data);
		}
		public function uder_del($id){
			$this->db->limit(1);
			$this->db->where('id', $id);
			return $this->db->delete('users');
		}
	}