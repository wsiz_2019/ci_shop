<?php 
class User extends CI_Controller{

	public function __construct(){
        parent::__construct();
    }
	public function index(){
		if(!$this->session->userdata('user')['logged_in']){
			$data['title'] = 'Logowanie';
			$this->load->view('user/header', $data);
			$this->load->view('user/start', $data);
		} else{
			redirect('backend/index');
		}
	}
	public function login(){
		$this->form_validation->set_rules('user', 'Login', 'required');
		$this->form_validation->set_rules('password', 'Hasło', 'required');

		if( $this->form_validation->run() === false ) {
			$data['title'] = 'Logowanie';
			$this->load->view('user/header', $data);
			$this->load->view('user/login', $data);
		} else {
			$user = $this->input->post('user');
			$user = str_replace(' ', '', $user);

			$password = $this->input->post('password');
			$userDB = $this->user_model->user_get(array('user' => $user));

			if(count($userDB) === 1 && password_verify($password, $userDB[0]['password'])){
				$userDB = reset($userDB);

				// Create session
				$user_data = array(
					'user' => array(
						'id' => $userDB['id'],
						'user' => $userDB['user'],
						'mail' => $userDB['mail'],
						'logged_in' => true,
					),
				);
				$this->session->set_userdata($user_data);
				$this->session->set_flashdata('success', 'Zalogowałeś się :)');
				redirect('backend/index','refresh');
			} else {
				$this->session->set_flashdata('danger', 'Błędna nazwa użytkownika lub hasło.');
				redirect('user/login');
			}
		}
	}
	public function start(){
		if(empty($this->session->userdata('user')['logged_in'])){
			redirect('user/login');
		}
	}
	public function logout(){
		$this->session->unset_userdata('user');
		$this->session->set_flashdata('danger', 'Zostałeś wylogowany.');
		redirect('user/login');
	}
}