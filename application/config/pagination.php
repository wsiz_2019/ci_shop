<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['num_links'] = 10;
$config['uri_segment'] = 3;
$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#"><strong>';
$config['cur_tag_close'] = '</strong></a></li>';
$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
$config['first_link'] = 'Pierwszy';
$config['last_link'] = 'Ostatni';
$config['attributes'] = array('class' => 'page-link');
// $config['base_url'] = '/ci/items/index';
// $config['total_rows'] = $this->db->get('item')->num_rows();// when I change this to `$config['total_rows'] = 200` it works
$config['total_rows'] = 200;
$config['per_page'] = 20;
$config['full_tag_open'] = '<div class="float-right"><ul class="pagination">';
$config['full_tag_close'] = '</ul></div>';
$config['first_tag_open'] = '<li class="prev page">';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li class="next page">';
$config['last_tag_close'] = '</li>';
$config['next_link'] = 'Dalej &rarr;';
$config['next_tag_open'] = '<li class="next page">';
$config['next_tag_close'] = '</li>';
$config['prev_link'] = '&larr; Wcześniej';
$config['prev_tag_open'] = '<li class="prev page">';
$config['prev_tag_close'] = '</li>';
$config['anchor_class'] = 'follow_link';